#import matplotlib.pyplot as plt
#from urllib.request import urlopen

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import time
import os
import re
from collections import Counter, deque

import cv2
import numpy as np
import tensorflow as tf
def prioritize(prioritized,top):
  for ID in top_k:
    if labels[ID] in prioritized:
      top = labels[ID]
      break
  return top

def printRecog(recog):
  if recog != '':
    print(recog)

def load_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()
    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)

    return graph

def load_labels(label_file):
    label = []
    proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
    for l in proto_as_ascii_lines:
        label.append(l.rstrip())
    return label

if __name__ == "__main__":
    model_file = "./mobilenet_greyscale_graph_2.pb"
    label_file = "./son_mobilenet_greyscale_labels.txt"
    input_height = 224
    input_width = 224
    input_mean = 128.0
    input_std = 128.0
    cap = cv2.VideoCapture(0)
    font = cv2.FONT_HERSHEY_PLAIN
    input_layer = "input"
    output_layer = "final_result"
    labels = load_labels(label_file)
    graph = load_graph(model_file)
    input_name = "import/" + input_layer
    output_name = "import/" + output_layer
    input_operation = graph.get_operation_by_name(input_name);
    output_operation = graph.get_operation_by_name(output_name);
    with tf.Session(graph=graph) as sess:
        counter = Counter()
        top = ''
        primar = {re.match(r'.+1$', label).group(0) for label in labels if re.match(r'.+1$', label)}
        primar.add('i')
        secondar = {re.match(r'.+3$', label).group(0) for label in labels if re.match(r'.+3$', label)}
        static = {letter for letter in "abcdefghiklmnopqrstuvwxy"}
        prioritizedS = {'n', 'r', 'm', 't', 'e', 's'}
        prioritizedD = {'z1', 'doctor1', 'doctor3', 'proud1', 'z3', 'impossible1', 'your1', 'go1', 'there1' , 'you1'}
        maxLen = 8
        deq = deque(maxlen = maxLen)
        recog = "START TIME : " + str(time.time())
        longString = ""
        while(True):
            ret, arr = cap.read()
            np_array_vid = cv2.cvtColor(arr, cv2.COLOR_BGR2GRAY)
            np_array_vid = cv2.cvtColor(np_array_vid,cv2.COLOR_GRAY2RGB)
            resized = cv2.resize(np_array_vid,(input_height, input_width))
            normalized = (resized - input_mean)/ input_std
            expanded = np.expand_dims(normalized, axis = 0)
            results = sess.run(output_operation.outputs[0],
                          {input_operation.outputs[0]: expanded})
            results = np.squeeze(results)
            top_k = results.argsort()[-5:][::-1]
            ID = top_k[0]
            top = labels[ID]
            #for ID in top_k:
              #longString += labels[ID] + ': ' + str(results[ID]) + '\n'
            #os.system('cls')
            #print(longString)
            longString = " "
            if top in static:
              top = prioritize(prioritizedS, top)
              deq.append(top)
              if deq.count(top) == 6 :
                recog = top
                deq.clear()
            elif top == "nothing":
              deq.clear()
            else:
              top = prioritize(prioritizedD, top)
              if top in primar:
                deq.append(top[:-1])
              else:
                if top[:-1] in deq:
                  recog = top[:-1]
                  deq.clear()
            printRecog(recog)
            recog = ''
            cv2.putText(np_array_vid, top, (10,250), font, 3,(0,255,0),2,cv2.LINE_AA)
            cv2.imshow('Frame',np_array_vid)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
              
        cap.release()
        cv2.destroyAllWindows()
