#Dynamic American Sign Language Recognition using retrained greyscale normalized MobileNET CNN
It can recognize 24 static alphabet gestures 28 dynamic gestures but you need white background and white t-shirt for it to work
You can find the list in the .txt file. You can reuse the code and algorithm in your project if you are not working in a controlled environment and even train your own modeels.
For their representation refer here :

[](https://www.handspeak.com/word/)

##Dependencies

A webcam or any usb cam.
Python > 3.5
tensorflow
opencv > 3.0.0
Numpy

    pip install python-opencv tensorflow numpy

##Clone repo

    git clone https://deduble@bitbucket.org/deduble/dynamicaslrecognition.git